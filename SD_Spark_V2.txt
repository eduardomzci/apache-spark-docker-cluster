- cd Descargas
Descomprime el archivo aqui
- cd data2/docker-spark-cluster
- docker-compose up --scale spark-worker=3
Espera a que termine Se activa el Master y 3 Esclavos

Ingresa al Buscador Firefox ingresa a: 127.0.0.1:9090/
Verifica la existencia de todo---------

Para acceder al Master: 
- docker exec -it df4a /bin/bash
Ficheros internos y conecciones
aqui crearemos la carpeta data:
- mkdir data
- cd data
-----> wget https://www.gutenberg.org/files/65289/65289-0.txt
-----> wget -O data.csv https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.csv?accessType=DOWNLOAD
- cd ..
Para ingresar al Shell o PySpark:
- cd spark/bin
- ./spark-shell
- ./pyspark

Aqui puede ejecutar todas la ordenes especificas que deees
df =spark.read.options(header='true', inferSchema='true').csv('/data/datae.csv')
df.count()
df.printSchema()
df.createOrReplaceTempView("ECOMERCE_DATA")
df2 = spark.sql("SELECT Description, Quantity, UnitPrice, Country from ECOMERCE_DATA where Country in ('United Kingdom','France','Australia')")
df2.printSchema()
df2.show()


Esto ejecutaras fuera de SparkShell o PySpark, "no dentro"
Para ejecutar con Submit como Aplicacion procederemos a:
-----> ./run-example --master spark://df4a9c31317a:7077 JavaWordCount /data/65289-0.txt
-----> ./spark-submit --master spark://df4a9c31317a:7077 --class org.apache.spark.examples.JavaWordCount ../examples/jars/spark-examples_2.11-2.4.3.jar /data/65289-0.txt


Verifica la ejecucion en el Cluster desde internet: 127.0.0.1:9090/
Entra a los esclavos y notaras que realizan las ejecuciones

FIN GRACIAS!!
